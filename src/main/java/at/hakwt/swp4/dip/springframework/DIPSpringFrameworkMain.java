package at.hakwt.swp4.dip.springframework;

import at.hakwt.swp4.dip.BusinessProcess;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class DIPSpringFrameworkMain {

    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("application-context.xml");
        BusinessProcess process = applicationContext.getBean("businessProcess", BusinessProcess.class);
        process.run();
    }

}
